import {createApp} from 'vue'
import App from './App.vue'
import settings from "@/settings";


const app = createApp(App)

// 配置路由
import router from './router'

app.use(router)

// 配置element
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

app.use(ElementPlus)

// 配置全局settings
app.config.globalProperties.$settings = settings // 配置全局变量


// 引入全局css
import "../static/css/reset.css"

// 引入axios
import axios from "axios";

axios.defaults.withCredentials = false // 阻止ajax 附带cookie
app.config.globalProperties.$axios = axios // 把axios 挂载到vue 的全局变量

app.mount('#app')