# @Time    : 2024/4/5 21:04
# @Author  : 🍁
# @File    : urls.py
# @Software: PyCharm

from django.urls import path, re_path
from .views import BannerListAPIView

urlpatterns = [
    path('banner/', BannerListAPIView.as_view()),
]
