# @Time    : 2024/4/5 20:58
# @Author  : 🍁
# @File    : serialzer.py
# @Software: PyCharm

from rest_framework import serializers
from .models import Banner


class BannerModelSerializer(serializers.ModelSerializer):
    """ 轮播图的序列化器 """

    class Meta:
        model = Banner
        fields = ["link", "image_url"]
