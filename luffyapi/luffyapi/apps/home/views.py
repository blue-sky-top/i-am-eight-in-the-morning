from django.shortcuts import render
from rest_framework.generics import ListAPIView
from .models import Banner

from .serialzer import BannerModelSerializer
from luffyapi.settings import constans

# Create your views here.


class BannerListAPIView(ListAPIView):
    queryset = Banner.objects.filter(is_show=True, is_delete=False).order_by("-orders", "-id")[:constans.BANNER_LENGTH]
    serializer_class = BannerModelSerializer
